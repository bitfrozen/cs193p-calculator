//
//  CSCLAppDelegate.h
//  CS193P-Calculator
//
//  Created by Antanas Domarkas on 2014-01-03.
//  Copyright (c) 2014 Curonian Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSCLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
