//
//  CSCLCalculatorBrain.h
//  CS193P-Calculator
//
//  Created by Antanas Domarkas on 2014-01-03.
//  Copyright (c) 2014 Curonian Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSCLCalculatorBrain : NSObject

- (void)pushOperand:(double)operand;
- (double)performOperation:(NSString *)operation;

@end
