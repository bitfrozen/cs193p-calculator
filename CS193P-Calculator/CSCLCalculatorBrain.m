//
//  CSCLCalculatorBrain.m
//  CS193P-Calculator
//
//  Created by Antanas Domarkas on 2014-01-03.
//  Copyright (c) 2014 Curonian Software. All rights reserved.
//

#import "CSCLCalculatorBrain.h"

#pragma mark - Private interface
@interface CSCLCalculatorBrain ()
@property(nonatomic, strong) NSMutableArray *operandStack;
@end

#pragma mark - Implementation
@implementation CSCLCalculatorBrain

#pragma mark - Public interface methods
- (void)pushOperand:(double)operand
{
    NSNumber *operandOject = [NSNumber numberWithDouble:operand];
    [self.operandStack addObject:operandOject];
}

- (double)performOperation:(NSString *)operation
{
    double result = 0;
    if ([operation isEqualToString:@"+"]) {
        result = [self popOperand] + [self popOperand];
    } else if ([operation isEqualToString:@"*"]) {
        result = [self popOperand] * [self popOperand];
    } else if ([operation isEqualToString:@"-"]) {
        double subtrahend = [self popOperand];
        result = [self popOperand] - subtrahend;
    } else if ([operation isEqualToString:@"/"]) {
        double divisor = [self popOperand];
        if (divisor) {
            result = [self popOperand] / divisor;
        }
    }

    [self pushOperand:result];
    return result;
}

#pragma mark - Private interface methods
- (double)popOperand
{
    NSNumber *operandObject = [self.operandStack lastObject];
    if (operandObject) {
        [self.operandStack removeLastObject];
    }

    return [operandObject doubleValue];
}

#pragma mark - Setter / getter methods
- (NSMutableArray *)operandStack
{
    if (!_operandStack) {
        _operandStack = [[NSMutableArray alloc] init];
    }

    return _operandStack;
}

@end
