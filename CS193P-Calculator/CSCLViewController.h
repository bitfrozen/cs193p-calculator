//
//  CSCLViewController.h
//  CS193P-Calculator
//
//  Created by Antanas Domarkas on 2014-01-03.
//  Copyright (c) 2014 Curonian Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSCLViewController : UIViewController

@property(weak, nonatomic) IBOutlet UILabel *display;

@end
