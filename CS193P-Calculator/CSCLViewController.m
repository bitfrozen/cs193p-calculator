//
//  CSCLViewController.m
//  CS193P-Calculator
//
//  Created by Antanas Domarkas on 2014-01-03.
//  Copyright (c) 2014 Curonian Software. All rights reserved.
//

#import "CSCLViewController.h"
#import "CSCLCalculatorBrain.h"

#pragma mark - Private interface
@interface CSCLViewController ()
- (IBAction)operationPressed:(id)sender;
- (IBAction)digitPressed:(id)sender;
@property(nonatomic) BOOL userIsInTheMiddleOfEnteringNumber;
@property(nonatomic, strong) CSCLCalculatorBrain *brain;
@end

#pragma mark - Implementation
@implementation CSCLViewController

#pragma mark - Private interface IBAction methods

- (IBAction)digitPressed:(UIButton *)sender
{
    NSString *digit = [sender currentTitle];
    if (self.userIsInTheMiddleOfEnteringNumber) {
        self.display.text = [self.display.text stringByAppendingString:digit];
    } else {
        self.display.text = digit;
        self.userIsInTheMiddleOfEnteringNumber = YES;
    }
}

- (IBAction)operationPressed:(UIButton *)sender
{
    if (self.userIsInTheMiddleOfEnteringNumber) {
        [self enterPressed];
    }
    double result = [self.brain performOperation:sender.currentTitle];
    NSString *resultString = [NSString stringWithFormat:@"%g", result];
    self.display.text = resultString;
}

- (IBAction)enterPressed
{
    [self.brain pushOperand:[self.display.text doubleValue]];
    self.userIsInTheMiddleOfEnteringNumber = NO;
}

#pragma mark - Setter / getter methods
- (CSCLCalculatorBrain *)brain
{
    if (!_brain) {
        _brain = [[CSCLCalculatorBrain alloc] init];
    }

    return _brain;
}

@end
