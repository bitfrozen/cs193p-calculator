//
//  main.m
//  CS193P-Calculator
//
//  Created by Antanas Domarkas on 2014-01-03.
//  Copyright (c) 2014 Curonian Software. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CSCLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CSCLAppDelegate class]));
    }
}
